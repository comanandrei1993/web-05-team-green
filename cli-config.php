<?php
/**
 * Created by PhpStorm.
 * User: Radu
 * Date: 9/16/2020
 * Time: 1:08 PM
 */

// cli-config.php
require_once "bootstrap.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);


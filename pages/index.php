<?php include('../components/header.php'); ?>
    <div class="row">
        <div class="col-12 px-0 text-center">
            <div class="header-background jumbotron w-100 d-flex ">
                <h1 class="heading-primary justify-content-center p-4 mx-auto">
                    <span class="heading-primary-main ">Green Team</span>
                    <span class="heading-primary-sub">Travel with us</span>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center my-5">
            <h2 class="heading-secondary my-5">Cine suntem noi?</h2>

            <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-4 card-deck ">
                <div class="col">
                    <div class="card bg-green hover-effect">
                        <img class=" card-img-top image-fit " src="../img/andrei.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title person-title">Andrei Coman</h5>
                            <p class="card-text person-description p-1">This is a wider card with supporting text below
                                as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>

                        <button type="button" class="btn btn-primary btn-lg  btn-style ">Andrei's journey</button>
                    </div>
                </div>

                <div class="col">
                    <div class="card bg-green hover-effect">
                        <img class=" card-img-top image-fit " src="../img/gabi.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title person-title">Gabriel Dobre</h5>
                            <p class="card-text person-description p-1">This is a wider card with supporting text below
                                as a
                                natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>

                        <button type="button" class="btn btn-primary btn-lg btn-block btn-style ">Gabriel's journey
                        </button>
                    </div>
                </div>

                <div class="col">
                    <div class="card bg-green hover-effect">
                        <img class=" card-img-top fit-image image-fit " src="../img/claudia.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title person-title">Claudia Pîrvu</h5>
                            <p class="card-text person-description p-1">This is a wider card with supporting text below
                                as a
                                natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>

                        <button type="button" class="btn btn-primary btn-lg btn-block btn-style ">Claudia's journey
                        </button>
                    </div>
                </div>

                <div class="col">
                    <div class="card bg-green hover-effect">
                        <img class=" card-img-top image-fit " src="../img/radu.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title person-title">Radu Căpitănescu</h5>
                            <p class="card-text person-description p-1">This is a wider card with supporting text below
                                as a
                                natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>

                        <button type="button" class="btn btn-primary btn-lg btn-block btn-style ">Radu's journey
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center my-5">
            <h2 class="heading-secondary my-5">green team's categories</h2>
        </div>
    </div>


    <div class="row ">
        <div class="col-12 mb-5">
            <div class="container">

                <div class="row">
                    <div class="col-12 col-lg-9">
                        <h2 class="heading-secondary text-center w-100 mt-5">Vlog de călătorie</h2>
                        <img class="article-img w-100 mt-2" src="../img/vlog_article.jpeg" alt="vlog article">
                        <h3 class="heading-tertiary">Experienţa mea turistică în Maldive</h3>
                        <p class="article-text">We were making our way to the Rila Mountains, where we were visiting the
                            Rila Monastery where we enjoyed scrambled eggs, toast, mekitsi, local jam and peppermint
                            tea. We wandered the site with busloads of other tourists, yet strangely the place did not
                            seem crowded. I’m not sure if it was the sheer size of the place, or whether the masses
                            congregated in one area and didn’t venture far from the main church, but I didn’t feel
                            overwhelmed by tourists in the monastery.</p>
                        <h2 class="heading-secondary text-center w-100 my-5">Obiective turistice</h2>
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-deck">
                                        <div class="card text-center">
                                            <img src="../img/buila-vanturarita.jpg"
                                                 class="card-img-top card-img-top-1 w-100" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-1 text-uppercase">
                                                    Buila-Vânturăriţa</h5>
                                                <p class="card-text card-text-1 ">Lorem ipsum dolor sit amet,
                                                    consectetur adipisicing elit. Assumenda dolorum exercitationem
                                                    mollitia odio quae veritatis voluptate! Deleniti dignissimos harum
                                                    placeat qui repudiandae similique ullam voluptatem.</p>
                                                <button type="button" class="btn btn-success btn-lg btn-block">Read
                                                    more...
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card text-center">
                                            <img src="../img/cheile_nerei.jpg" class="card-img-top card-img-top-1 w-100"
                                                 alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-1 text-uppercase">Cheile Nerei</h5>
                                                <p class="card-text card-text-1">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Assumenda dolorum exercitationem mollitia odio
                                                    quae veritatis voluptate! Deleniti dignissimos harum placeat qui
                                                    repudiandae similique ullam voluptatem.</p>
                                                <button type="button" class="btn btn-success btn-lg btn-block">Read
                                                    more...
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <h2 class="heading-secondary text-center w-100 my-5">Suveniruri</h2>
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-deck">
                                        <div class="card text-center">
                                            <img src="../img/suvenir_2.jpg" class="card-img-top card-img-top-1 w-100"
                                                 alt="..." title="Farfurii decorative">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-1 text-uppercase">Farfurii
                                                    decorative</h5>
                                                <p class="card-text card-text-1 ">Lorem ipsum dolor sit amet,
                                                    consectetur adipisicing elit. Assumenda dolorum exercitationem
                                                    mollitia odio quae veritatis voluptate! Deleniti dignissimos harum
                                                    placeat qui repudiandae similique ullam voluptatem.</p>
                                                <button type="button" class="btn btn-success btn-lg btn-block">Read
                                                    more...
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card text-center">
                                            <img src="../img/suvenir_1.jpg" class="card-img-top card-img-top-1 w-100"
                                                 alt="..." title="Cutiuţe handmade pictate">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-1 text-uppercase">Cutiuţe handmade
                                                    pictate</h5>
                                                <p class="card-text card-text-1">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Assumenda dolorum exercitationem mollitia odio
                                                    quae veritatis voluptate! Deleniti dignissimos harum placeat qui
                                                    repudiandae similique ullam voluptatem.</p>
                                                <button type="button" class="btn btn-success btn-lg btn-block">Read
                                                    more...
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php include "../components/sidebar.php"; ?>
                </div>
            </div>
        </div>
    </div>

<?php include('../components/footer.php'); ?>
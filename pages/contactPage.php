<?php include "../components/header.php"; ?>

<div class="row">
    <div class="col-12 px-0 text-center">
        <div class="header-background jumbotron w-100 d-flex ">
            <h1 class="heading-primary justify-content-center p-4 mx-auto">
                <span class="heading-primary-main ">Green Team</span>
                <span class="heading-primary-sub">Travel with us</span>
            </h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 p-0">
            <img class="img-fluid" src="../img/headerImg.jpeg" align="center">
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-9 myBlack-shadow">
            <div class="row">
                <div class="col-12 mt-5 text-Contact">
                    <p>
                        <b> Green Team</b> , este blogul ideal unde-ti poti gasi cu usurinta urmatoarea destinatie de
                        vacanta. Lansat in august 2020, <b>Green team</b> a devenit cel mai de incredere si cunoscut
                        blog pentru organizarea vacantelor, fiind recunoscut de nenumarate ori in presa si de
                        comunitate.
                    </p>
                </div>

                <div class="col-12 text-Contact">
                    <p>
                        Succesul acestui blog se datoreaza veridicitatii fiecarei postari.Fiecare postare in parte
                        reprezinta o noua experinta traita de catre unul din cei patru membri ai echipei.
                    </p>
                </div>

                <div class="col-12 mt-3">
                    <h2>
                        <b>
                            Pentru sugestii sau propuneri de colaborare, ne puteti contacta folosind formularul de mai
                            jos.
                        </b>
                    </h2>
                </div>

                <?php
                if (!empty($errors)){
                    var_dump($errors);
                }
                ?>
                <?php include "../components/ContactMe.php"; ?>
            </div>
        </div>

        <?php include "../components/sidebar.php"; ?>

    </div>
</div>

<?php include "../components/footer.php"; ?>


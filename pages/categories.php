<?php include('../components/header.php'); ?>

<div class="row">
    <div class="col-12 categories-background">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-7 my-5">
                    <nav aria-label="breadcrumb ">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item breadcrumb-style"><a class="white-text" href="index.php">Home</a></li>
                            <li class="breadcrumb-item breadcrumb-style"><a class="white-text" href="#">Categorie</a></li>
                            <li class="breadcrumb-item active text-white breadcrumb-style"  aria-current="page">Vlog de călătorie</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-3"></div>
            </div>

            <div class="row">
                <div class="col-2"></div>
                <div class="col-6 my-5">
                    <h1 class="text-white text-uppercase display-3 font-weight-bold mb-3">
                        Vlog de călătorie
                    </h1>
                    <p class="font-italic category-description text-white mb-0">
                        Descoperă cele mai tari locuri din lume pe care le poți vizita. Îți punem la dispoziție o serie de obiective turistice interesante, îți spunem ce activități poți întreprinde și nu în ultimul rând te îndrumăm în alegerea celor mai bune suveniruri
                    </p>
                    <img src="../img/travel_objects.png" alt="traveling" title="Green Team">

                </div>
                <div class="col-4">
                    <img src="../img/aparat.png" alt="vlog de calatorie" title="vlog de calatorie">
                </div>
            </div>




        </div>
    </div>
</div>
<div class="row ">
    <div class="col-12 my-5">
        <div class="container">

            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="card mb-5 w-100">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="../img/american.png" class="card-img" alt="vlog in america" title="Vlog în America">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title heading-secondary">vlog în america</h5>
                                    <p class="card-text vlog-card-text ">Vrei să călătoreşti pe continentul american şi nu ştii cum te poţi distra mai bine sau ce poţi vizita?Avem cele mai tari articole şi poze de la faţa locului.Începe să trăieşti visul american de aici!</p>
                                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase btn-style mt-5 ">Visul american</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3 w-100">
                        <div class="row no-gutters">
                            <div class="col-md-4 d-flex">
                                <img src="../img/asian.png" class="card-img align-self-end" alt="vlog în Asia"  title="Vlog în Asia">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title heading-secondary">vlog în asia</h5>
                                    <p class="card-text vlog-card-text">Mâncare asiatică, meditaţie, yoga, vizitarea templelor budiste şi hinduse, vizitarea oraşelor în care stau călugării budişti şi să nu uităm de Marele Zid Chinezesc, construcţia vizibilă de pe Lună!Descoperă Asia împreună cu Green Team!</p>
                                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase btn-style mt-5 ">Namaste!</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3 w-100">
                        <div class="row no-gutters">
                            <div class="col-md-4 d-flex">
                                <img src="../img/europa.png" class="card-img align-self-end" alt="vlog în Europa"  title="Vlog în Europa">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body h-100 d-flex align-items-start flex-column">
                                    <h5 class="card-title heading-secondary">vlog în europa</h5>
                                    <p class="card-text vlog-card-text mb-auto">Cultura europeană este mai diversificată decât v-aţi putea imagina! Toate religiile şi tradiţiile lumii se regăsesc pe bătrânul continent.Alături de Green Team veţi descoperi Europa aşa cum nu o ştiaţi până acum!Berlin,Paris,Milano,Roma sunt la picioarele voastre!Să nu uităm nici de ţara noastră!Aţi fi surprinşi să aflaţi că turiştii străini ar da orice pentru a vizita un colţ de rai de prin cătunurile ţării noastre! </p>
                                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase btn-style mt-5 ">Europa, bătrânul continent..</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3 w-100">
                        <div class="row no-gutters">
                            <div class="col-md-4 d-flex">
                                <img src="../img/african.png" class="card-img align-self-end" alt="vlog în Africa"  title="Vlog în Asia">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title heading-secondary">vlog în asia</h5>
                                    <p class="card-text vlog-card-text">Viaţa pe continentul african diferă de la regiune la regiune.Mergând din nordul continentului către sud vei vedea două extreme specifice acestui continent: sărăcia cruntî şi bunăstarea materială.Veniţi cu noi într-o călătorie uimitoare pe continentul african!</p>
                                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase btn-style mt-5 ">Continentul negru</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include "../components/sidebar.php"; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../components/footer.php'); ?>

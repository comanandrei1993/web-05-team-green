<?php include('../components/header.php'); ?>

<div class="row">
    <div class="col-12 subcategory-background">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-7 my-5">
                    <nav aria-label="breadcrumb ">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item breadcrumb-style"><a class="white-text" href="index.php">Home</a></li>
                            <li class="breadcrumb-item breadcrumb-style"><a class="white-text" href="categories.php">Categorie</a></li>
                            <li class="breadcrumb-item breadcrumb-style"><a class="white-text" href="categories.php">Vlog de călătorie</a></li>
                            <li class="breadcrumb-item active text-white breadcrumb-style" aria-current="page">Vlog în America</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-3"></div>
            </div>

            <div class="row">
                <div class="col-2"></div>
                <div class="col-6 my-5">
                    <h1 class="text-white text-uppercase display-3 font-weight-bold mb-3">
                        Vlog în America
                    </h1>
                    <p class="font-italic category-description text-white mb-0">
                        Living the american dream! Metropola New York, Los Angeles (oraşul îngerilor), Las Vegas, oraşul în care lumea nu doarme, Marele Canion, Cascada Niagara, mirificul Texas cu celebrii cowboy..Noi am trăit visul american! Este rândul vostru să o faceţi cu ajutorul nostru!
                    </p>
                    <img src="../img/aparat.png" alt="traveling" title="Green Team">

                </div>
                <div class="col-4">
                    <img src="../img/burger.png" alt="vlog de calatorie" title="vlog de calatorie">
                </div>
            </div>




        </div>
    </div>
</div>
<div class="row ">
    <div class="col-12 my-5">
        <div class="container">

            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="container-fluid w-100">
                        <div class="row">
                            <div class="col-6">
                                <div class="card w-100 text-center " style="width: 18rem;">
                                    <img class="card-img-top card-img-top-1" src="../img/articles/marele_canyon.jpg" alt="Marele Canyon">
                                    <div class="card-body">
                                        <h5 class="card-title card-title-1 text-uppercase">Marele Canyon</h5>
                                        <p class="card-text card-text-1">Grand Canyon este un parc național aflat pe teritoriul Statelor Unite, în nord-vestul Arizonei. Caracteristica centrală a parcului este Grand Canyon, un defileu al râului Colorado, care este una dintre cele șapte minuni naturale ale lumii.</p>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item list-group-item-style text-left pl-5">Categorie: <strong class="text-success">Vlog de călătorie</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Subcategorie: <strong class="text-success">Vlog în America</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Autor: <strong class="text-success">Căpitănescu Radu</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Dată: <strong class="text-success">3 septembrie 2020</strong></li>
                                    </ul>
                                    <div class="card-body">
                                        <button type="button" class="btn btn-success btn-lg btn-block">Citeşte articolul...
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card w-100 text-center " style="width: 18rem;">
                                    <img class="card-img-top card-img-top-1" src="../img/articles/marele_canyon.jpg" alt="Marele Canyon">
                                    <div class="card-body">
                                        <h5 class="card-title card-title-1 text-uppercase">Marele Canyon</h5>
                                        <p class="card-text card-text-1">Grand Canyon este un parc național aflat pe teritoriul Statelor Unite, în nord-vestul Arizonei. Caracteristica centrală a parcului este Grand Canyon, un defileu al râului Colorado, care este una dintre cele șapte minuni naturale ale lumii.</p>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item list-group-item-style text-left pl-5">Categorie: <strong class="text-success">Vlog de călătorie</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Subcategorie: <strong class="text-success">Vlog în America</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Autor: <strong class="text-success">Căpitănescu Radu</strong></li>
                                        <li class="list-group-item list-group-item-style text-left pl-5">Dată: <strong class="text-success">3 septembrie 2020</strong></li>
                                    </ul>
                                    <div class="card-body">
                                        <button type="button" class="btn btn-success btn-lg btn-block">Citeşte articolul...
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5 d-flex">
                            <div class="col-3">
                                <nav aria-label="...">
                                    <ul class="pagination pagination-text">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                                        </li>
                                        <li class="page-item"><a class="page-link text-success" href="#">1</a></li>
                                        <li class="page-item active">
                                            <a class="page-link bg-success" href="#">2 <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="page-item"><a class="page-link text-success" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link text-success" href="#">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-3 pagination-text d-flex align-self-center justify-content-end ">
                                Pagina 1 din 2
                            </div>
                        </div>

                    </div>







                </div>

                <?php include "../components/sidebar.php"; ?>
            </div>
        </div>
    </div>
</div>

<?php include('../components/footer.php'); ?>


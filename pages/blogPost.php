<!------------------------------------>
<!------------  PHP CODE  ------------>
<!------------------------------------>
<?php
//////////////////////
////// INCLUDES /////
////////////////////
include '../functions/functions.php';

//////////////////////
////// CLASSES //////
////////////////////

/// Project Classes ///
// BlogPost class
/** @var App\Entity\BlogPost $blogPost */

// User Class
/** @var App\Entity\User $author */

// Comment Class
/** @var App\Entity\Comment $comment */
/////////////////////////////////////////////

/// Library Classes ///

//////////////////////////////

////// TWIG //////

//////////////////////////////
$blogPost = App\Entity\BlogPost::find($_GET['id']);

$commentators = [];
foreach ($blogPost->getComments() as $comment) {
    $commentators[] =  App\Entity\User::find($comment->getUserId());
}
?>

<!----------------------------------------------->
<!------------  INCLUDE HTML HEADER  ------------>
<!----------------------------------------------->
<?php
include '../components/header.php';
?>

<div class="row">
    <div class="col-12 px-0 text-center">
        <div class="header-background jumbotron w-100 d-flex ">
            <h1 class="heading-primary justify-content-center p-4 mx-auto">
                <span class="heading-primary-main ">Green Team</span>
                <span class="heading-primary-sub">Travel with us</span>
            </h1>
        </div>
    </div>
</div>

<main class="container-fluid container">
    <div class="row">
        <!----------------------------------------------->
        <!------------  BLOG POST GOES HERE  ------------>
        <!----------------------------------------------->
        <div class="col-12 col-lg-9">
            <section class="row myBlogPost">

                <!-- Article Template here -->
                <?php echo $twig->render('parts/blogPostArticle.html.twig', array('blogPost' => $blogPost)); ?>

            </section>

            <!-------- Recommended Posts -------->
            <!----------------------------------->
            <section class="row mt-5 recommendedPosts">
                <div class="col-12 col-md-6 col-lg-4 recommendedPostCard">
                    <img src="../img/blog-post-covers/blog-post-1-cover.jpg" alt="recommended post image"
                         class="col-12 recommendedPostCard__img">

                    <h5 class="col-12 mt-4 mb-0 font-weight-bold myFont-size-14 recommendedPostCard__postTitle">
                        Lorem Ipsum</h5>

                    <p class="col-12 myFont-size-12 myDate-color recommendedPostCard__postDate">Jancember 33rd,
                        2020</p>
                </div>

                <div class="col-12 col-md-6 col-lg-4 recommendedPostCard">
                    <img src="../img/blog-post-covers/blog-post-1-cover.jpg" alt="recommended post image"
                         class="col-12 recommendedPostCard__img">

                    <h5 class="col-12 mt-4 mb-0 font-weight-bold myFont-size-14 recommendedPostCard__postTitle">Lorem
                        Ipsum</h5>

                    <p class="col-12 myFont-size-12 myDate-color recommendedPostCard__postDate">Jancember 33rd, 2020</p>
                </div>

                <div class="col-12 col-md-6 col-lg-4 recommendedPostCard">
                    <img src="../img/blog-post-covers/blog-post-1-cover.jpg" alt="recommended post image"
                         class="col-12 recommendedPostCard__img">

                    <h5 class="col-12 mt-4 mb-0 font-weight-bold myFont-size-14 recommendedPostCard__postTitle">Lorem
                        Ipsum</h5>

                    <p class="col-12 myFont-size-12 myDate-color recommendedPostCard__postDate">Jancember 33rd, 2020</p>
                </div>
            </section>

            <!-------- Post Comments -------->
            <!------------------------------->
            <section class="row mt-5 postComments">
                <h3 class="col-12 font-weight-bold text-center myFont-size-14 postComments__nrOfComments">
                    <u>
                        <?php
                        $nrOfComments = count($blogPost->getComments());
                        if ($nrOfComments == 1) {
                            echo $nrOfComments.' comentariu';
                        } else {
                            echo $nrOfComments.' comentarii';
                        }
                        ?>
                    </u>
                </h3>

                <!-- Article Comment Section -->
                <?php echo $twig->render(
                        'parts/blogPostArticleComments.html.twig',
                        array(
                            'comments' => $blogPost->getComments(),
                            'commentators' => $commentators
                        )
                ); ?>

<!--                <div id="postComments" class="col-12 mt-3 postComments__commentContainer">-->
<!--                    --><?php //foreach ($blogPost->getComments() as $comment): ?>
<!--                        <div class="row">-->
<!--                            <img class="col-2 postComments__userImg" src="" alt="user image">-->
<!---->
<!--                            <div class="col-10 postComments__comment">-->
<!--                                <div class="mb-2 postComments__commentHeader">-->
<!--                                    <span class="font-weight-bold myFont-size-12 postComments__commentAuthor">-->
<!--                                        --><?php //echo App\Entity\User::find($comment->getUserId())->getName(); ?>
<!--                                    </span>-->
<!---->
<!--                                    <span class="myFont-size-12 myDate-color postComments__commentDate">-->
<!--                                        --><?php //echo $comment->getDate(); ?>
<!--                                    </span>-->
<!---->
<!--                                    <a href="#"-->
<!--                                       class="font-weight-bold text-dark myFont-size-12 postComments__commentReply">Reply-->
<!--                                    </a>-->
<!--                                </div>-->
<!---->
<!--                                <p class="myFont-size-14 postComments__commentText">-->
<!--                                    --><?php //echo $comment->getText(); ?>
<!--                                </p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
            </section>

            <!-------- Leave a Comment Form -------->
            <!-------------------------------------->
            <section class="row mt-5 leaveComment">
                <h4 class="col-12 text-center myFont-size-14 leaveComment__disclaimer">
                    Your email address will not be published. Required fields are marked with *
                </h4>

                <form class="col-12 leaveComment__form">
                    <div class="row formGroup">
                        <label class="col-12 font-weight-bold myFont-size-14" for="commentText">Comment:</label>
                        <textarea class="col-12 px-3 py-2 myFont-size-14 leaveComment__textarea"
                                  name="commentText"
                                  id="commentText"
                                  cols="30"
                                  rows="10"></textarea>
                    </div>

                    <div class="row">
                        <div class="col-4 myFont-size-14 pl-0 formGroup">
                            <label class="font-weight-bold" for="commentAuthor">Name:*</label>
                            <input class="d-block w-100 px-3 py-2 leaveComment__input"
                                   name="commentAuthor"
                                   id="commentAuthor"
                                   type="text"
                                   placeholder="Name*">
                        </div>

                        <div class="col-4 myFont-size-14 pl-0 formGroup">
                            <label class="font-weight-bold" for="commentEmail">Email:*</label>
                            <input class="d-block w-100 px-3 py-2 leaveComment__input"
                                   name="commentEmail"
                                   id="commentEmail"
                                   type="email"
                                   placeholder="Email*">
                        </div>
                    </div>

                    <button id="postComment" class="row myFont-size-14 mt-4 mb-5 btn btn-dark">Post comment</button>
                </form>
            </section>
        </div>
        <!------------------------------------------------>
        <!------------------------------------------------>
        <!------------------------------------------------>

        <!------------------------------------------------>
        <!------------  ASIDE MENU GOES HERE  ------------>
        <!------------------------------------------------>
        <?php include "../components/sidebar.php" ?>
        <!------------------------------------------------>
        <!------------------------------------------------>
        <!------------------------------------------------>
    </div>
</main>

<!----------------------------------------------->
<!------------  INCLUDE HTML FOOTER  ------------>
<!----------------------------------------------->
<?php
include '../components/footer.php';
?>

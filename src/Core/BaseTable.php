<?php

namespace App\Core;

abstract class BaseTable {
    protected $id;

    abstract function getTable();

    /**
     * BaseTable constructor.
     */
    public function __construct($data) {
        foreach ($data as $column => $value) {
            $this->$column = $value;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function save() {
        if ($this->id) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    /////////////////////////////////
    //////////// CREATE ////////////
    ///////////////////////////////
    public function insert() {
        global $mysqli;
        $table = static::getTable();
        $data = get_object_vars($this);
        $columnNames = [];
        $valuesNames = [];

        foreach ($data as $column => $value) {
            $columnNames[] = mysqli_real_escape_string($mysqli, $column);
            $valuesNames[] = mysqli_real_escape_string($mysqli, $value);
        }

//    var_dump("INSERT INTO `web-05-team-green`.`".$table."` (`".implode("`, `", $columnNames)."`) VALUES ('".implode("', '", $valuesNames)."')");die;
        $query = mysqli_query(
            $mysqli,
            "INSERT INTO `web-05-team-green`.`".$table."` (`".implode("`, `", $columnNames)."`) VALUES ('".implode("', '", $valuesNames)."')"
        );

        $this->id = mysqli_insert_id($mysqli);
    }


    ///////////////////////////////
    //////////// READ ////////////
    /////////////////////////////

    // findBy method -> takes an array as parameter like ['columnName' => $value] //
    public static function findBy($filters) {
        global $mysqli;
        $table = static::getTable();

        $criterias = [];
        foreach ($filters as $column => $value) {
            $criterias[] = "`".$column."`='".mysqli_real_escape_string($mysqli, $value)."'";
        }

//        var_dump("SELECT * FROM `".$table."` WHERE ".implode('AND', $criterias));
        $query = mysqli_query($mysqli, "SELECT * FROM `".$table."` WHERE ".implode('AND', $criterias));

        if ($query == false) {
            die('SQL error:'.mysqli_error($mysqli));
        }

        $dbData = $query->fetch_all(MYSQLI_ASSOC);

        $result = [];
        foreach ($dbData as $data) {
            $class = static::class;
            $result[] = new $class($data);
        }

        return $result;

    }

    // findByLike method -> takes an array as parameter like ['columnName' => $value] //
    // returns rows from database as array[objects] //
    public static function findByLike($filters) {
        global $mysqli;

        $table = static::getTable();

        $criterias = [];
        foreach ($filters as $column => $value) {
            $criterias[] = "`".$column."` LIKE '".mysqli_real_escape_string($mysqli, $value)."'";
        }

//        var_dump("SELECT * FROM `".$table."` WHERE ".implode('AND', $criterias));
        $query = mysqli_query($mysqli, "SELECT * FROM `".$table."` WHERE ".implode('AND', $criterias).";");

        if ($query == false) {
            die('SQL error:'.mysqli_error($mysqli));
        }

        $dbData = $query->fetch_all(MYSQLI_ASSOC);

        $result = [];
        foreach ($dbData as $data) {
            $class = static::class;
            $result[] = new $class($data);
        }

        return $result;

    }

    // findByOne method -> takes an array as parameter like ['columnName' => $value] //
    //  returns 1 row from database as object //
    public static function findOneBy($filters) {
        $result = self::findBy($filters);

        if (count($result) > 0) {
            return $result[0];
        }

        return false;
    }

    // find method -> takes a number as parameter //
    //  returns 1 row from db as object //
    public static function find($id) {
        $result = self::findOneBy(['id' => $id]);

        if (isset($result)) {
            return $result;
        }

        return false;
    }

    /////////////////////////////////
    //////////// UPDATE ////////////
    ///////////////////////////////
    public function update() {
        global $mysqli;
        $table = static::getTable();
        $data = get_object_vars($this);

        $columnNames = [];
        $valueNames = [];

        foreach ($data as $column => $value) {
            $columnNames[] = mysqli_real_escape_string($mysqli, $column);
            $valueNames[] = mysqli_real_escape_string($mysqli, $value);
        }

//var_dump("UPDATE `web-05-team-green`.`".$table."` SET `".implode(', ', $columnNames)."`='".mysqli_real_escape_string($mysqli, implode(', ' , $valueNames))."' WHERE  `id`=".intval($this->id).";");

        $query = mysqli_query(
            $mysqli,
            "UPDATE `web-05-team-green`.`".$table."` SET '".implode(', ', $columnNames)."'='".mysqli_real_escape_string($mysqli, implode(', ' , $valueNames))."' WHERE  `id`=".intval($this->id).";"
        );
    }

//    private function update() {
//        global $mysqli;
//        $table = static::getTable();
//        $data = get_object_vars($this);
//        $myUpdate = pairColWithVal($data);
//
////    var_dump("UPDATE `".$table."` SET ".implode(',', $myUpdate)." WHERE ".implode('AND', $criterias));
//        $query = mysqli_query($mysqli, "UPDATE `".$table."` SET ".implode(',', $myUpdate)." WHERE id=".intval($this->id));
//    }


    /////////////////////////////////
    //////////// DELETE ////////////
    ///////////////////////////////
    public function delete() {
        global $mysqli;
        $table = static::getTable();

//        DELETE FROM `web-05-team-green`.`blog_post` WHERE  `id`=1;
        $query = mysqli_query(
            $mysqli,
            "DELETE FROM `web-05-team-green`.`".$table."` WHERE  'id'=".$this->id.";"
        );
}
}















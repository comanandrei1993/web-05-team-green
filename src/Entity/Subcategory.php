<?php
namespace App\Entity;
use App\Core;
/**
 * Created by PhpStorm.
 * User: Radu
 * Date: 9/7/2020
 * Time: 2:26 PM
 */
class Subcategory extends Core\BaseTable
{

    protected $subCategory_name;

    protected $subCategory_image;

    protected $subCategory_description;

    protected $subCategory_image_object_right;

    protected $subCategory_image_object_left;

    protected $subCategory_posts_nr;


    public function getTable()
    {
        return 'subcategories';
    }




    ////// Getters //////
    /**
     * @return mixed
     */


    public function getCategoryName()
    {
        return $this->subCategory_name;
    }

    public function getEmail()
    {
        return $this->subCategory_image;
    }

    public function getPassword()
    {
        return $this->subCategory_description;
    }

    public function getStatus()
    {
        return $this->subCategory_image_object_right;
    }

    public function getAuthorDescription()
    {
        return $this->subCategory_image_object_left;
    }

    public function getPicture()
    {
        return $this->subCategory_posts_nr;
    }
}
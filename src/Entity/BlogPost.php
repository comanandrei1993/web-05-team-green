<?php

namespace App\Entity;
use App\Core;

/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 01-Sep-20
 * Time: 9:43 AM
 */

class BlogPost extends Core\BaseTable {
    protected $post_title;

    protected $category_id;

    protected $post_text_header;

    protected $post_text;

    protected $picture;

    protected $author_id;

    protected $date;

    public function getTable() {
        return 'blog_post';
    }

    ////// Get object data methods //////
    public function getPostTitle() {
        return $this->post_title;
    }

    public function getCategoryId() {
        return $this->category_id;
    }

    public function getPostTextHeader() {
        return $this->post_text_header;
    }

    public function getPostText() {
        return $this->post_text;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function getAuthor() {
        return User::find($this->author_id);
    }

    public function getDate() {
        return date('jS F, Y', strtotime($this->date));
    }
    /////////////////////////////////////////////////////////////

    public function getComments() {
        return Comment::findBy(['post_id' => $this->id]);
    }
}

















<?php
namespace App\Entity;

/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 04-Sep-20
 * Time: 2:26 PM
 */


class CommentTemplate {
    /** @var  User $user */
    protected $username;

    protected $date;

    /** @var  Comment $comment */
    protected $text;

    /**
     * commentTemplate constructor.
     * @param User $user
     * @param Comment $comment
     */
    public function __construct(User $user, Comment $comment) {
        $this->username = $user->getName();

        $this->text = $comment->getText();

        $this->date = $comment->getDate();
    }

    public function buildNewComment() {
        return '
<div class="row">
    <img class="col-2 postComments__userImg" src="" alt="user image">

    <div class="col-10 postComments__comment">
        <div class="mb-2 postComments__commentHeader">
            <span class="font-weight-bold myFont-size-12 postComments__commentAuthor">'.$this->username.'</span>

            <span class="myFont-size-12 myDate-color postComments__commentDate">'.$this->date.'</span>

            <a href="#" class="font-weight-bold text-dark myFont-size-12 postComments__commentReply">Reply</a>
        </div>

        <p class="myFont-size-14 postComments__commentText\">'.$this->text.'</p>
    </div>
</div>
';
}
}
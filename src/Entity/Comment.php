<?php

namespace App\Entity;
use App\Core;
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 02-Sep-20
 * Time: 2:34 PM
 */
class Comment extends Core\BaseTable {
    protected $post_id;

    protected $user_id;

    public $date;

    protected $text;

    public function getTable() {
        return 'comments';
    }

    ////// GET object data methods //////
    public function getPostId() {
        return $this->post_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getDate() {
        return date('F jS, Y \l\a h:i A', strtotime($this->date));
    }

    public function getText() {
        return $this->text;
    }
    ////////////////////////////////////////////////////////////////////////

    ////// SET object data methods //////
    public function setDate($date) {
            $this->date = $date;
    }
}
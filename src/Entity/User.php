<?php
namespace App\Entity;
use App\Core;

/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 02-Sep-20
 * Time: 2:49 PM
 */
class User extends Core\BaseTable {
    protected $name;

    protected $email;

    protected $password;

    protected $status;

    protected $description;

    protected $picture;

    protected $phone;

    protected $user_message;

    public function getTable() {
        return 'users';
    }

    ////// Get object data methods //////
    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getUserMessage() {
        return $this->user_message;
    }


    /////////////////////////////////////////////////////////////


}
<?php

namespace App\Entity;
use App\Core;
/**
 * Created by PhpStorm.
 * User: Radu
 * Date: 9/3/2020
 * Time: 4:18 PM
 */
class Category extends Core\BaseTable
{
    protected $parent_id;

    protected $category_name;

    protected $subCategory_name;

    protected $category_image;

    protected $description;

    protected $category_image_object_right;

    protected $category_image_object_left;

    protected $category_posts_nr;


    public function getTable()
    {
        return 'categories';
    }

    ////// Setters //////
    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id) {
        $this->parent_id = $parent_id;
    }

    ////// Getters //////
    /**
     * @return mixed
     */
    public function getParentId() {
        return $this->parent_id;
    }

    public function getCategoryName()
    {
        return $this->category_name;
    }

    public function getSubCategoryName()
    {
        return $this->subCategory_name;
    }

    public function getEmail()
    {
        return $this->category_image;
    }

    public function getPassword()
    {
        return $this->description;
    }

    public function getStatus()
    {
        return $this->category_image_object_right;
    }

    public function getAuthorDescription()
    {
        return $this->category_image_object_left;
    }

    public function getPicture()
    {
        return $this->category_posts_nr;
    }
}

<?php
namespace App\Entity;
use App\Core;
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 01-Sep-20
 * Time: 7:30 PM
 */
class Subscriber extends Core\BaseTable {
    protected $email;

    public function getTable() {
        return 'subscribers';
    }

    ////// Get data methods //////
    public function getEmail() {
        return $this->email;
    }
    //////////////////////////////////

}
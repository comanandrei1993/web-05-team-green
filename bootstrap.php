<?php
/**
 * Created by PhpStorm.
 * User: Radu
 * Date: 9/16/2020
 * Time: 12:41 PM
 */

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Entities"), $isDevMode, null, null, false);

// database configuration parameters
$connectionParams = array(
    'dbname' => 'web-05-team-green',
    'user' => 'root',
    'password' => 'scoalait123',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
);
$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);



// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
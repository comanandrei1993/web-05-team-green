


<div class="col-12">

    <div class="contact-section ">
        <h1>Contact Us</h1>
        <div class=""></div>
        <form class="contact-form w-100" action="../components/form_process.php" method="post">
            <input type="text" name="username" class="contact-form-text myFont-size-14" placeholder="Your name" required >
            <input type="email" name="email" class="contact-form-text myFont-size-14" placeholder="Your email" required >
            <input type="tel" id="phone" name="phone" pattern="[0-9]{4}-[0-9]{3}-[0-9]{3}"  class="contact-form-text myFont-size-14" placeholder="Your phone" required>
            <input type="text" name="subject" class="contact-form-text myFont-size-14" placeholder="Subiect" required >
            <textarea class="contact-form-text myFont-size-14" name="description" required placeholder="Your message" ></textarea>
            <input type="submit" class="contact-form-btn  myFont-size-14" name="submit" value="Send">
        </form>
    </div>
</div>



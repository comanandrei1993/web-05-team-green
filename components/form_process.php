<?php

include "../vendor/autoload.php";
/**
 * Created by PhpStorm.
 * User: Radu
 * Date: 9/3/2020
 * Time: 6:22 PM
 */

$nameErr = $emailErr = $contactErr = "";
$name = $email = $contact = "";
$errors = [];



if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Name validation:
    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $errors[] = $nameErr;
    } else {
        $name = test_input($_POST["name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            $nameErr = "Puteţi folosi doar litere şi spaţii";
            $errors[] = $nameErr;
        }
    }

    // Mail validation:
    if (empty($_POST["email"])) {
        $emailErr = "Vă rugăm să introduceţi adresa de email";
        $errors[] = $emailErr;
    } else{
    $email = test_input($_POST["email"]);
    if (!preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $email)) {
        $emailErr = "Adresa de mail introdusă nu este validă";
        $errors[] = $emailErr;

    }
}
    // Mobile validation:
    if (empty($_POST["contact"])) {
        $contactErr = "Numărul de telefon este obligatoriu";
        $errors[] = $contactErr;
    } else {
        $contact = test_input($_POST["contact"]);
        // check if contact number is well-formed
        if (!preg_match("/^[0-9+]*$/",$contact)) {
            $contactErr = "Phone number should contain only numbers";
            $errors[] = $contactErr;

        }
    }

}
if (!empty($errors)){
    include "../pages/contactPage.php";
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if(isset($_POST)) {
    $userName = $_POST['name'];
    $userEmail = $_POST['email'];
    $messageSubject = $_POST['subject'];
    $message = $_POST['description'];

    $to = "capitanescu.radu@yahoo.com";

    $bodyEmail = "";

    $body .= "From:> " . $userName . "\r\n";
    $body .= "Email:> " . $userEmail . "\r\n";
    $body .= "Message:> " . $message . "\r\n";

    mail($to, $messageSubject, $body);

    $mailSent= mail($to, $messageSubject, $body);

}

if (isset($mailSent)) {


// Create the Transport
    $transport = (new Swift_SmtpTransport('smtp.webfaction.com', 25))
        ->setUsername('radumail')
        ->setPassword('radu12345678')

// Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

// Create a message
    $message = (new Swift_Message('Mesaj trimis cu succes!'))
        ->setFrom(['capitanescu.radu@yahoo.com' => 'Capitanescu Radu'])
        ->setTo(['$userEmail'])
        ->setBody("<p style='margin-bottom:10px;'>Salutare <span><?php echo '$userName'; ?></span>! Îți mulțumim pentru <span style='color: red;text-transform: uppercase;'>formularul</span> trimis și te asigurăm că îți vom răspunde într-un timp cât mai scurt!</p> ','text/html");

// Send the message
    $result = $mailer->send($message);
}
?>
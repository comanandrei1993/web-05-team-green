
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--------------------------------------------->
    <!----- jQuery and bootstrap requirements ----->
    <!--------------------------------------------->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <!--------------------------------------------->
    <!----------------- Bootstrap ----------------->
    <!--------------------------------------------->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!--------------------------------------------->
    <!--------------- Font Awesome ---------------->
    <!--------------------------------------------->
    <script src="https://kit.fontawesome.com/eefb7a1525.js" crossorigin="anonymous"></script>
    <!--------------------------------------------->
    <!--------------- Google Fonts ---------------->
    <!--------------------------------------------->
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
    <!--------------------------------------------->
    <!--------------- Custom Styles --------------->
    <!--------------------------------------------->
    <link rel="stylesheet" href="../css/styles.css">
    <!--------------------------------------------->
    <!---------------- Page Script ---------------->
    <!--------------------------------------------->
    <script src="../javascript/script.js"></script>
</head>
<body>

<div class="container-fluid ">
    <div class="row  mx-5 my-5">
        <a href="#"> <i class="fab fa-facebook-f header-icon px-3 myFbIcon header-icon-link"></i></a>
        <a href="#"> <i class="fab fa-twitter-square header-icon px-3 header-icon-link"></i></a>
        <a href="#"> <i class="fab fa-pinterest header-icon px-3 header-icon-link"></i></a>
        <a href="#"> <i class="fab fa-instagram header-icon px-3 header-icon-link"></i></a>
    </div>

    <div class="row ">
        <div class="col-12 ">
            <div class="container ">
                <div class="row">
                    <div class="col-4"></div>
                    <div class="col-4 ">
                        <a class="w-50" href="../pages/index.php">
                            <img class="mx-auto d-block " src="../img/logo.png" alt="logo" title="Green Team Logo">
                        </a>
                    </div>
                    <div class="col-4"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 w-100">
            <hr>
        </div>
    </div>

    <div class="row ">
        <div class="col-12">
            <div class="container ">
                <div class="row">
                    <div class="col-12 text-center">
                        <nav class="navbar navbar-expand-lg">
                            <div class="collapse navbar-collapse d-flex " id="navbarNavDropdown">
                                <ul class="navbar-nav  navigation-list w-100 justify-content-center ">
                                    <li class="nav-item active p-3">
                                        <a class="nav-link navigation-link text-warning" href="index.php">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item dropdown p-3">
                                        <a class="nav-link dropdown-toggle navigation-link" href="#"
                                           id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                            Categorie
                                        </a>
                                        <ul class="dropdown-menu navigation-list" aria-labelledby="navbarDropdownMenuLink">
                                            <li class="nav-link dropdown-submenu">
                                                <a class="dropdown-item dropdown-toggle navigation-link" href="#" >Vlog de călătorie</a>
                                                <ul class="dropdown-menu navigation-list">
                                                    <li><a class="dropdown-item navigation-link" href="subcategory.php" target="_blank">Vlog în Europa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="subcategory.php" target="_blank">Vlog în Asia</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="subcategory.php" target="_blank">Vlog în Africa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="subcategory.php" target="_blank">Vlog în America</a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-link dropdown-submenu">
                                                <a class="dropdown-item dropdown-toggle navigation-link" href="#" >Obiective turistice</a>
                                                <ul class="dropdown-menu navigation-list">
                                                    <li><a class="dropdown-item navigation-link" href="#">Obiective turistice Europa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Obiective turistice Asia</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Obiective turistice Africa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Obiective turistice America</a></li>
                                                </ul>
                                            </li>

                                            <li class="nav-link dropdown-submenu">
                                                <a class="dropdown-item dropdown-toggle navigation-link" href="#" >Suveniruri</a>
                                                <ul class="dropdown-menu navigation-list">
                                                    <li><a class="dropdown-item navigation-link" href="#">Suveniruri Europa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Suveniruri Asia</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Suveniruri Africa</a></li>
                                                    <li><a class="dropdown-item navigation-link" href="#">Suveniruri America</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item p-3">
                                        <a class="nav-link navigation-link" href="#">Despre Noi</a>
                                    </li>
                                    <li class="nav-item p-3">
                                        <a class="nav-link navigation-link" href="contactPage.php">Contact</a>
                                    </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
//////////////////////
////// INCLUDES /////
////////////////////
include '../functions/functions.php';


//////////////////////
////// CLASSES //////
////////////////////
// Comment Class
/** @var \App\Entity\Comment $comment */

// User Class
/** @var \App\Entity\User $user */
//////////////////////////////////////////


////// Create $user variable //////
if (User::findBy(['email' => $_POST['email']])) {
    $user = User::findOneBy(['email' => $_POST['email']]);
} else {
    $user = new User([
        'email' => $_POST['email'],
        'name' => $_POST['name'],
        'status' => 'user'
    ]);
    $user->save();
}


////// Create new comment //////
$comment = new \App\Entity\Comment([
    'post_id' => $_POST['post_id'],
    'user_id' => $user->getId(),
    'text' => $_POST['text'],
]);
$comment->setDate(date('Y-m-d H:i:s', mktime()));
$comment->save();

$response = [];

$newComment = new \App\Entity\CommentTemplate($user, $comment);
$response['html'] = $newComment->buildNewComment();

echo json_encode($response);














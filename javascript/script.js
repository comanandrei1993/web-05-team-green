/**
 * Created by andrei on 02-Sep-20.
 */
$(document).ready(function () {
    $('#postComment').on('click', function (e) {
        e.preventDefault();
        let urlParams = new URLSearchParams(window.location.search);

        const commentObj = {
            post_id: urlParams.get('id'),
            text: $('#commentText').val(),
            name: $('#commentAuthor').val(),
            email: $('#commentEmail').val(),
        };

        $.post(
            'http://188.240.210.8/web-05/andrei/andrei/blog-team-green/processes/process-add-new-post-comment.php',
            commentObj,
            function (resp) {
                let respObj = JSON.parse(resp);
                console.log(respObj);

                $('#postComments').append(respObj.html);

                $('#commentText').val('');
                $('#commentAuthor').val('');
                $('#commentEmail').val('');
            }
        );
    });

    /* make Dropdown work inside another dropdown */
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        let $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass("show");
        });

        return false;
    });
});






